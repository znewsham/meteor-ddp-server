# Deep merge

Allows you to specify that a specific subscription/publication should use a "deep merge" - e.g., allows the same client to subscribe to multiple non top-level fields and still function correctly. Ignoring the obvious security problems of this publication, here's an example:

```js
//server.js
Meteor.publish("users", function (search, fields) {
  // some security here
  this.deepMerge = true;
  return Meteor.users.find(search, { fields });
});

//client.js
const sub1 = Meteor.subscribe("users", { _id: Meteor.userId() }, { "profile.firstName": 1 });
const sub2 = Meteor.subscribe("users", { _id: Meteor.userId() }, { "profile.lastName": 1 });
Tracker.autorun(() => {
    if (sub1.ready() && sub2.ready()) {
      console.log(Meteor.users.findOne({ _id: Meteor.userId() }));
    }
})
```

# Future dev

1. Right now, the entire document is still sent to the client on change - this is to maintain the original functionality on the client. In the future I'd like to change this.

2. It would be nice to specify which fields to be deep on: `this.deepMerge = ["profile"]`


# ddp-server
[Source code of released version](https://github.com/meteor/meteor/tree/master/packages/ddp-server) | [Source code of development version](https://github.com/meteor/meteor/tree/devel/packages/ddp-server)
***
